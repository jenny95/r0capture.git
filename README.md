## 安卓应用层抓包通杀脚本的安装和使用
原项目地址：https://github.com/r0ysue/r0capture  
1. **安装frida运行依赖库及frida-tools**
```shell script
pip install -i https://mirrors.aliyun.com/pypi/simple/ numpy matplotlib
pip install -i https://mirrors.aliyun.com/pypi/simple/ frida-tools
```
2. **安装frida**
```shell script
pip install -i https://mirrors.aliyun.com/pypi/simple/ frida
```
3. **下载安装frida-server**  
查看安卓手机内核
```shell script
getprop ro.product.cpu.abi
```
根据内核下载[frida-server](https://github.com/frida/frida/releases)

4. **运行frida-server**
```shell script
adb push frida-server /data/local
adb shell
cd /data/local
chmod 777 frida-server
./frida-server &
```
5. **转发android TCP端口到本地**
```shell script
adb forward tcp:27042 tcp:27042
adb forward tcp:27043 tcp:27043
```
6. **下载安卓应用层抓包通杀脚本**
[r0capture](https://gitee.com/jenny95/r0capture.git)  

**简介**
- 仅限安卓平台，测试安卓7、8、9、10 可用 ；
- 无视所有证书校验或绑定，不用考虑任何证书的事情；
- 通杀TCP/IP四层模型中的应用层中的全部协议；
- 通杀协议包括：Http,WebSocket,Ftp,Xmpp,Imap,Smtp,Protobuf等等、以及它们的SSL版本；
- 通杀所有应用层框架，包括HttpUrlConnection、Okhttp1/3/4、Retrofit/Volley等等；
7. **r0capture使用方法**
- Spawn 模式：

`$ python r0capture.py -U -f com.videogo`

- Attach 模式，抓包内容保存成pcap文件供后续分析：

`$ python r0capture.py -U com.videogo -p videogo.pcap`

建议使用`Attach`模式，从感兴趣的地方开始抓包，并且保存成`pcap`文件，供后续使用Wireshark进行分析  
**执行后结果**  
![wireshark](https://img-blog.csdnimg.cn/20210112134801107.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2NTEzNzk0,size_16,color_FFFFFF,t_70)
![报文](https://img-blog.csdnimg.cn/20210112134820413.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2NTEzNzk0,size_16,color_FFFFFF,t_70)